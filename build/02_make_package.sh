#!/bin/bash

set -eu

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")

readonly PROJECT_DIR=$(readlink -f "${PROGDIR}/..")
readonly SRC_DIR="${PROJECT_DIR}/src"
readonly PACKAGE_DIR="${SRC_DIR}/package"
readonly PERMS_FILE="${SRC_DIR}/permissions"

set_file_permissions() {
    for f in $(find "$PACKAGE_DIR" -mindepth 1 | sed "s|^${PROJECT_DIR}/||")
    do
        local file_config_line=$(cat "${PERMS_FILE}" | egrep "^[^#].* $f\$")
        if [[ -z "$file_config_line" ]]
        then
            echo "Not configuration for '$f' was found in '${PERMS_FILE}'"
            exit 1
        fi
        local octal_perms=$(echo "${file_config_line}" | cut -d ' ' -f 1)
        local owner=$(echo "${file_config_line}" | cut -d ' ' -f 2)

        local dst_file="${PROJECT_DIR}/$f"
        chmod "${octal_perms}" "${dst_file}"
        chown "${owner}" "${dst_file}"
    done
}

make_package() {
    cd "${SRC_DIR}"
    dpkg-deb --build "${PACKAGE_DIR}"
}

rename_package() {
    cd "${SRC_DIR}"
    local pkg_file="$(basename "${PACKAGE_DIR}").deb" # package has same name as directory it's build from
    local canonical_name=$(dpkg-deb --showformat='${Package}_${Version}_${Architecture}.deb' --show "${pkg_file}")
    mv "${pkg_file}" "${canonical_name}"
}

main() {
    set_file_permissions #git does not keep permissions and ownership
    make_package
    rename_package
}

main
