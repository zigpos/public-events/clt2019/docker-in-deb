#!/bin/bash

set -eu

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")

readonly PROJECT_DIR=$(readlink -f "${PROGDIR}/..")
readonly SRC_DIR="${PROJECT_DIR}/src"
readonly PACKAGE_DIR="${SRC_DIR}/package"

main() {
    rm -f "${SRC_DIR}/*.deb"
    rm -f "${PACKAGE_DIR}/var/lib/docker_packages/*.tar.gz"
}

main