#!/bin/sh
# we can't use bash in the shebang line, as the standard docker image for docker does not include bash

set -eu

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")

readonly PROJECT_DIR=$(readlink -f "${PROGDIR}/..")
readonly SRC_DIR="${PROJECT_DIR}/src"
readonly PACKAGE_DIR="${SRC_DIR}/package"

readonly IMAGE_NAME="zigpos/hello-world-nginx:2019a"
readonly IMAGE_ARCHIVE="/var/lib/docker_images/nginx_hello_world.tar.gz"

get_docker_image() {
    docker pull "${IMAGE_NAME}"
}

export_docker_image() {
    mkdir -p "${PACKAGE_DIR}/var/lib/docker_images"
    docker save "${IMAGE_NAME}" | gzip -c > "${PACKAGE_DIR}/${IMAGE_ARCHIVE}"
}

main() {
    get_docker_image
    export_docker_image
}

main
