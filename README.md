# Docker Image in Debian Package

This repository contains the sources for a simple Docker image in a Debian package.
Upon installation, the package will set up a Docker container using the image to expose a simple "hello world" website on port 80.

## Building the Package

Simply run the scripts in the `build` folder:

```bash
$ build/01_get_docker_image.sh
$ build/02_make_package.sh
```  


The first script needs access to the Docker daemon (you may have to run it with `sudo`, depending on your setup).

The second script fixes file permissions for the packaged files, which may require root privileges.
It also calls `dkpg-deb`, which may not be available in your distribution (e.g. because you run Arch Linux).
The simplest way to get around this is to run the script in a Docker container like this:

```bash
$ docker run -it --rm -v "$PWD:/mnt" debian:9 /usr/bin/dpkg-deb --build /mnt/src/package
```

## Installing the package

Copy the package to the target system and run

```bash
$ apt install <path-to-package.deb>
```

You may have to prefix the package name with `./` if it's in the same directory, otherwise apt would not understand that
it should install from a file instead of searching the repositories.

## Test the installation

Visit port 80 on the target machine, it should display a simple hello world page.
